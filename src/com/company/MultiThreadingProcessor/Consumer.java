package com.company.MultiThreadingProcessor;

/**
 * Created by root on 10.06.17.
 */
public class Consumer {

    private static int result;

    public static void save(int x, int y, int z){


        Thread currentThread = Thread.currentThread();
        System.out.println(currentThread.getClass() + " go to synch block");

        synchronized (currentThread.getClass()) {

            if (currentThread instanceof Cubator){
                System.out.println("    Cubator start work");
            }

            if (currentThread instanceof Kvadrator){
                System.out.println("    Kvadrator start work");
            }

            if (currentThread instanceof Prostator){
                System.out.println("    Prostator start work");
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            result = result + x + y + z;
            System.out.println("Result = " + result);
        }
    }

}
