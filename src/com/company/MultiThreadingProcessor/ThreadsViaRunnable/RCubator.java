package com.company.MultiThreadingProcessor.ThreadsViaRunnable;
/**
 * Created by root on 10.06.17.
 */
public class RCubator implements Runnable {

    private int x;

    public RCubator(int x) {
        this.x = x;
    }

    @Override
    public void run() {
        int result = x * x * x;
        RConsumer.save(result, 0, 0, this);
        System.out.println("    Cubator end work");
    }
}
