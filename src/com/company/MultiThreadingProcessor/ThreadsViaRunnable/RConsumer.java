package com.company.MultiThreadingProcessor.ThreadsViaRunnable;

/**
 * Created by root on 10.06.17.
 */
public class RConsumer {

    private static int result;

    public static void save(int x, int y, int z, Runnable inst){


        //Thread currentThread = Thread.currentThread();
        System.out.println(inst.getClass() + " go to synch block");

        synchronized (inst.getClass()) {

//            if (currentThread instanceof RCubator){
//                System.out.println("    Cubator start work");
//            }
//
//            if (currentThread instanceof RKvadrator){
//                System.out.println("    Kvadrator start work");
//            }
//
//            if (currentThread instanceof RProstator){
//                System.out.println("    Prostator start work");
//            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            result = result + x + y + z;
            System.out.println("Result = " + result);
        }
    }
}
