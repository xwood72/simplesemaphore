package com.company.MultiThreadingProcessor.ThreadsViaRunnable;

/**
 * Created by root on 10.06.17.
 */
public class RKvadrator implements Runnable {

    private int y;

    public RKvadrator(int y) {
        this.y = y;
    }

    @Override
    public void run() {
        int result = y * y;
        RConsumer.save(0, result, 0, this);
        System.out.println("    Kvadrator end work");

    }
}
