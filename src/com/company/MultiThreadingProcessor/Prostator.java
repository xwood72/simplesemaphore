package com.company.MultiThreadingProcessor;

/**
 * Created by root on 10.06.17.
 */
public class Prostator extends Thread{

    private int z;

    public Prostator(int z) {
        this.z = z;
    }

    @Override
    public void run() {
        Consumer.save(0,0, z);
        System.out.println("    Prostator end work");

    }

}
