package com.company.MultiThreadingProcessor;

/**
 * Created by root on 10.06.17.
 */
public class Cubator extends Thread {

    private int x;

    public Cubator(int x) {
        this.x = x;
    }

    @Override
    public void run() {
        int result = x * x * x;
        Consumer.save(result, 0, 0);
        System.out.println("    Cubator end work");
    }

}
