package com.company.MultiThreadingProcessor;

/**
 * Created by root on 10.06.17.
 */
public class Kvadrator extends Thread{

    private int y;

    public Kvadrator(int y) {
        this.y = y;
    }

    @Override
    public void run() {
        int result = y * y;
        Consumer.save(0, result, 0);
        System.out.println("    Kvadrator end work");

    }

}
