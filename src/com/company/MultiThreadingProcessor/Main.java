package com.company.MultiThreadingProcessor;

import com.company.MultiThreadingProcessor.ThreadsViaRunnable.RCubator;
import com.company.MultiThreadingProcessor.ThreadsViaRunnable.RKvadrator;
import com.company.MultiThreadingProcessor.ThreadsViaRunnable.RProstator;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] x = {1, 2 ,3};
        int[] y = {4, 5 ,6};
        int[] z = {7, 8 ,9};

        for (int i = 0; i < x.length; i++) {
            (new Cubator(x[i])).start();
            (new Kvadrator(y[i])).start();
            (new Prostator(z[i])).start();

//            RCubator rc = new RCubator(x[i]);
//            RKvadrator rk = new RKvadrator(y[i]);
//            RProstator rp = new RProstator(z[i]);
//
//            (new Thread(rc)).start();
//            (new Thread(rk)).start();
//            (new Thread(rp)).start();
        }
    }
}
